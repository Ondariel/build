module.exports = {  

  'should display a success create account message' : function (browser) {
    var registerLink = "a[href^='registerPage'";
    var btnSubmit = "button[id=submit]";
    var facebookConnectLink = "a[href^='connexionPage']";
    browser
      .url('http://www.subyourvids.com')
      .waitForElementVisible('body', 1000)
      .pause(1000)
      .click(registerLink)
      .pause(1000)
      .setValue('input[name=nickname]', 'nightwatch')
      .pause(1000)
      .setValue('input[name=firstname]', 'night')
      .pause(1000)
      .setValue('input[name=name]', 'watch')
      .pause(1000)
      .setValue('input[name=email]', 'test@nightwatch.com')
      .pause(1000)
      .setValue('input[name=password]', 'nightwatch')
      .pause(1000)
      .setValue('input[name=password2]', 'nightwatch')
      .pause(1000)
      .click(btnSubmit)
      .pause(2000)
      .assert.containsText('#messages_success', "Votre compte vient d'être créé.")
      .pause(1000)
      .end();
  },

'should display an unactive submit button' : function (browser) {
    var registerLink = "a[href^='registerPage']";
    var btnSubmit = "button[id=submit]";
    browser
      .url('http://www.subyourvids.com')
      .pause(1000)
      .click(registerLink)
      .pause(1000)
      .assert.cssClassPresent(btnSubmit, "disabled")
      .end();
  },

'should display error field and label in red' : function (browser) {
    var registerLink = "a[href^='registerPage']";
    //var labelNickname = "label[for=nickname]";
    var inputNickname = "input[name=nickname]";
    browser
      .url('http://www.subyourvids.com')
      .pause(1000)
      .click(registerLink)
      .pause(1000)
      .setValue(inputNickname, 'nightwatch')
      .clearValue(inputNickname)
      .pause(1000)
      .assert.cssClassPresent(inputNickname, "has-error")
      //.assert.cssClassPresent(labelNickname, "has-error")
      .end();
  }

};