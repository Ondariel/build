module.exports = {  

  'should log the user and redirect to home page' : function (browser) {
    var connexionLink = "a[href^='connexionPage']";
    var userLink = "a[id=user_link]";
    var logoutLink = "a[id=logout_link]";
    var btnSubmit = "button[id=btn_signin]";
    var url = 'http://www.subyourvids.com/';
    browser
      .url(url)
      .waitForElementVisible('body', 1000)
      .pause(1000)
      .click(connexionLink)
      .pause(1000)
      .setValue('input[name=nickname]', 'nightwatch')
      .pause(1000)
      .setValue('input[name=password]', 'nightwatch')
      .pause(1000)
      .click(btnSubmit)
      .pause(1000)
      .assert.urlEquals(url)
      .pause(1000)
      .assert.visible(userLink)
      .assert.visible(logoutLink)
      .end();
  },

'should log the user with facebook account and redirect to home page' : function (browser) {
    var connexionLink = "a[href^='connexionPage'";
    var btnFacebookConnect = ".fb_iframe_widget";
    var btnSubmit = "button[id=btn_signin]";
    browser
      .url('http://www.subyourvids.com')
      .waitForElementVisible('body', 1000)
      .pause(1000)
      .click(connexionLink)
      .pause(5000)
      .click(btnFacebookConnect)
      /*.setValue('input[name=nickname]', 'nightwatch')
      .pause(1000)
      .setValue('input[name=password]', 'nightwatch')
      .pause(1000)
      .click(btnSubmit)*/
      .pause(10000)
      .end();
  }

};